/**
 * Created by minht on 7/6/2017.
 */
Template.activities.helpers({
    getPastEvent: function () {

        let isPast = ( date ) => {
            let today = moment().format();
            return moment( today ).isAfter( date );
        };
        var allEvents = Events.find().fetch().map((event)=> {
            event.isNew = !isPast( event.start );
            // console.log("shit");
            return event;
        });
        return allEvents;
    }

});