
Router.configure({
    layoutTemplate: 'home',

});
Router.route("/",{
    name: "intro",
    template: "intro"
});

Router.route("/about",{
    name: "about",
    template: "about"
});

Router.route("/activities",{
    name:"",
    template:"activities"
});
Router.route("/repos",{
    name:"repos",
    template:"repos"
});
Router.route("/calendar",{
    name:"calendar",
    template:"calendar"
});