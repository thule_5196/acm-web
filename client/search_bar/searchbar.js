Template.search_result.helpers({
    searchEvents: function () {
        var regexp = new RegExp(Session.get('search/keyword'), 'i');
        if (regexp == "#####################"){
            console.log("empty");
            Session.set('search/keyword', "#####################");

            return [];
        } else{
            return Events.find({title: regexp}).fetch();
        }
    }
});

Template.navbar.events({
    'load #search': function(event) {
        Session.set('search/keyword', "#####################");
            console.log("empty");

    },
    'keyup #search': function(event) {
        Session.set('search/keyword', event.target.value);
        if (event.target.value == ""){
            console.log("empty");
            Session.set('search/keyword', "#####################");
        }
        document.getElementById("search_result").style.display="block";
    }

});/**
 * Created by minht on 7/13/2017.
 */
