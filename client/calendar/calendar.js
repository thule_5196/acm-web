/**
 * Created by minht on 7/7/2017.
 */
let isPast = ( date ) => {
    let today = moment().format();
    return moment( today ).isAfter( date );
};
Template.calendar.onCreated( () => {
    let template = Template.instance();
    template.subscribe( 'events' );
});

Template.calendar.onRendered( () => {
    $( '#calendar' ).fullCalendar({
        events( start, end, timezone, callback ) {
            let data = Events.find().fetch().map( ( event ) => {
                // event.editable = !isPast( event.start );
                event.editable = false;
                return event;
            });

            if ( data ) {
                callback( data );
            }
        }
    });

    Tracker.autorun( () => {
        Events.find().fetch();
        $( '#calendar' ).fullCalendar( 'refetchEvents' );
    });
});